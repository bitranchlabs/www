FROM alpine as builder
RUN apk add --update-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/community/ zola
WORKDIR /www
COPY . .
RUN zola build

FROM nginx
COPY --from=builder /www/public/ /usr/share/nginx/html
