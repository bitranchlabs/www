---
date: 2021-11-29
title: "Hello From Forestry! \U0001F332\U0001F44B"

---
This post was made with [forestry.io](https://forestry.io "Forestry.io"). Forestry is a CMS for git-based statically generated sites -- though I suspect it'll work with _any_ git repository that contains a markup language. I'm not sure I'll use this full-time, but I do like the option and could see it being useful if I want to write a post while I'm not near a terminal. I'm most doing this demo for my wife though, who would like to start blogging but:

1. Wants the blog writing process to be easy
2. Doesn't want to use git and markdown
3. Has a husband who refuses to admin a Wordpress or Ghost site

So far the only issue I'm facing is getting Forestry's preview function to play nicely with Zola. Not a deal breaker, I don't need to preview my posts, but maybe she might, and it does make sense to use the service to its full extent.

I'll report back if/when I figure out the preview setup. 

But other than that the process is seamless so far. Oauth with my gitlab account, point Forestry at the blog repo, tell it where the content directories live, setup some frontmatter templates, and BOOM! 🚀

If this blog shows up without any edits, you'll know that final test worked -- actually publishing content 😂