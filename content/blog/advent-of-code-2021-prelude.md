+++
title = "Advent of Code 2021: Prelude"
date = "2021-11-28"
[taxonimies]
tags = ["aoc", "racket"]
+++

The 2021 Advent of Code starts in a few days and I have been thinking about
what I wanted to do this year. I tried the 2019 AoC in Rust. 2018 and 2020 I
did in Python. This year I had thought about doing the challenges in Lua. But
then I had the idea of just doing each day in a different langauge. It appears
this isn't a novel idea, people have been doing it since the very first AoC in
2015. The term coined for this is "Polyglot of Code". For someone who hasn't
ever even finished all 25 days of AoC, this seems like it might be too great of
a challenge, but I like to think I _could_ finish and just get sidetracked
around the 20th day with the holiday season...

This is the list of languages (in no particular order) that I have thought of
using for my polyglot of code  challenge. I may or may not use all the
languages on this list.

 - Python
 - Vlang
 - Rust
 - C
 - Java
 - Javascript/Typescript
 - Ruby/Crystal
 - Go
 - Bash
 - Fish
 - Nim
 - A lisp clojure/racket/scheme/sbcl/elisp
 - Haskell
 - Forth
 - Lua
 - Elixir
 - Erlang
 - Awk
 - Julia
 - Pyth
 - PHP
 - Perl
 - Ocaml
 - Kotlin
 - Scala
 - R
 - Cpp
 - Odin
 - Zig

 I also plan to provide a Dockerimage for each day/lang so I don't need 25
 different languages on my machine to run my solutions.

 I'm thinking I'll want to save my go-to langs for later on. Forth with
 probably be one of the first languages I reach for, since Forth isn't merely
 another language, it's another language on another planet! I think I'll leave
 the following langauges in reserve, for when the going gets tough:

 - Python
 - Vlang
 - Rust
 - Javascript/Typescript
 - Go
 - Lua

 I'm also a big hesitant about using both Go and Vlang. I'm not using both Ruby
 _and_ Crystal because the syntaxes are so similar, so I might need to
 reconsider Vlang as well, though in Vlang's favor it is it's own language,
 unrelated to Go. I'd also love to get some more exciting and foreign langauges
 in, but I'm not sure what and I'm also interested in finished this challenge
 before the 2022 one starts!

 See you on December 1st!
