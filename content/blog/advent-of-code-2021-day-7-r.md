+++
title = "Advent of Code 2021: Day 7 -- R"
date = "2021-12-07"
+++

I dislike R.

Anywho, today's problem was pretty simple. Part one is merely the sum of a
vector minus the median of the vector. One nice thing about R is its functions
all work natively with vector-like objects, which meant I could literally just
write `sum(abs(median(data) - data))` and it would figure that out for me. Most
other langauges would require that I iterate over the values in `data` and run
the `abs(median(data) - foo)` for each value, then collect and sum that. So,
I'll give R that much. But... docs suck, searching for help sucks -- "R" has
got to be one of the worst SEO terms in existence, at least in the top 26!

Part two was a little more involved. I actually stumbled upon the answer -- or
the almost answer -- immediately by switching `median` out for `mean`. However
it worked for the _test data_ but not my _input data_. Turns out if I swapped a
`ceiling` for a `floor` it would work for the _input_ but not the _test_. I
just ended up calculating both and then asking for the minimum value. Not sure
if this is the best way, there might be some way to calculate the target more
  optimally, but hey if it works!

I don't have much of an explanation for today's, it's not exactly hard and I
more or less explained it... So without further ado:

```R
data <- t(read.table("input.txt",header=FALSE,sep=","))
fuel <- sum(abs(median(data) - data))
print(fuel)

target_a <- ceiling(mean(data))
target_b <- floor(mean(data))
used <- function(x) x * (x + 1) / 2
fuel <- min(sum(used(abs(target_a - data))), sum(used(abs(target_b - data))))
print(fuel)
```

One thing of note, that `used` function. I came upon that by accident. I was
originally doing `sum(1:x)`which works, but the range on the _input_ gets so
large that R says "that's too much, I'm going to only take _some_ of those
elements". So I then had to come up with a function to calculate the cost
intead. Annoying, I thought my trick with a range was a neat solution...
