+++
title = "Advent of Code 2021: Day 8 -- Python and A Change"
date = "2021-12-08"
+++
First the change, I have decided to cease the polyglot aspect of this year's
Advent of Code. Why? I'm sick of not using languages I want to use and then
spending a majority of my time languishing over what language _to_ use.
Removing the polyglot aspect doesn't mean I'm going to just write Python or
Rust or something for the rest of the advent, but it does mean I'm not going to
sweat using the same language more than once. What I am going to attempt to do
though is to _not_ reach for Python for every day. I've had my Python fun, and
I often reach for it to demo out the solution anyway, but I am still going to
make a concerted effort to use things other than Python, but now I can reach
for Go, Rust, JavaScript, Vlang, and other things I _want_ to use without
  worrying about using them too soon into the advent and than being stuck with
  langauges I am not familiar with when the challenges get hard.

Onto the snake...

This challenge was probably the first fun one of this year. I enjoyed looking
at seven segment displays and trying to figure out the logic to determine what
set of "wires" matches which number. I also decided to write very OOPy Python,
which is actually fun sometimes!

```python
class Segment:
    def __init__(self, definitions, wires):
        self.wires = set(wires)
        self.defintions = definitions
        self.kind = None
        kind = [k for k, v in self.defintions.items() if v == self.wires]
        self.kind = kind[0] if kind else None
```

We'll start from the top. `Segment` is a fairly generic class. All it really
does is take a dictionary containing the definitions of all potential segments
and then the wires for the current segment and determines which number the
wires represent. We use a set here, because I found that the order of wires
wasn't consistent.

```python
class Display:
    def __init__(self, raw_definitions, segments):
        self.raw_definitions = raw_definitions.split()
        self.raw_segments = segments
        self.definitions = self.parse_definitions()
        self.segments = self.parse_segments()
        self.output = int(''.join(str(x.kind) for x in self.segments))
```

Onto the main class, `Display`, which does the real work for this solution. It
takes two strings, one is the definitions of segments and then the other is the
four segments in the display. We parse the definitions, than determine which
segments are in the `Display` and finally come up with the value of `output`,
which is just all four segments as an integer.

```python
    def parse_definitions(self):
        one = set([x for x in self.raw_definitions if len(x) == 2][0])
        four = set([x for x in self.raw_definitions if len(x) == 4][0])
        seven = set([x for x in self.raw_definitions if len(x) == 3][0])
        eight = set([x for x in self.raw_definitions if len(x) == 7][0])
        nine = set([x for x in self.raw_definitions if len(x)
                   == 6 and len(four - set(x)) == 0][0])
```

This definition is the bulk of `Display`. We grab the known displays (1, 4, 7,
and 8) and then we figure out the rest from there. Nine is the easiest to
determine because it's the 6-wire segment that contains all wires in common
with the four segment definition.

```python
        zero = set([x for x in self.raw_definitions if len(x)
                    == 6 and set(x) != nine and len(one - set(x)) == 0][0])
        six = set([x for x in self.raw_definitions if len(x)
                   == 6 and set(x) != nine and set(x) != zero][0])
```

Once we known nine we can find the zero, because it's the only other 6-wire
segment that shares all wires with one. Then six is the only other 6-wire
display.

```python
        three = set([x for x in self.raw_definitions if len(x) == 5 and len(
            set(x) - nine) == 0 and len(one - set(x)) == 0][0])
```

Three is a 5-wire segment, all of it's wires are present in nine, and it shares
ones wires as well.

```python
        five = set([x for x in self.raw_definitions if len(x)
                    == 5 and len(six - set(x)) == 1][0])
```

Five is also a 5-wire segment and it shares all but one wire in common with
six, which is a unqiue trait among the 5-wire digits.

```python
        two = set([x for x in self.raw_definitions if len(x)
                   == 5 and set(x) != five and set(x) != three][0])
        return {1: one, 2: two, 3: three, 4: four, 5: five,
                6: six, 7: seven, 8: eight, 9: nine, 0: zero}
```

Finally, two is the only remaining 5-wire display. Than we return a dictionary using the literal digit as the key and the _set_ of wires as the values.

```python
    def parse_segments(self):
        segments = []
        for segment in self.raw_segments.split():
            segments.append(Segment(self.definitions, segment))
        return segments
```

This method is trivial. It creates four `Segment` classes based on the string
input. It passes the newly discovered definitions as well as the wire configure
for this current segment, and then lets the `Segment` class do the work of
  determining which it is.

```python
    def count_segment_kinds(self):
        counter = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 0: 0}
        for segment in self.segments:
            if segment.kind:
                counter[segment.kind] += 1
        return counter
```

This method is used for part 1. It returns the number of each digit seen in the
display.

```python
def main(file_name):
    input = [*open(file_name)]
    displays = []
    for line in input:
        displays.append(Display((l := line.split(" | "))[0], l[1]))
    print("Part 1: ", sum([v for x in displays for k,
          v in x.count_segment_kinds().items() if k in [1, 4, 7, 8]]))
    print("Part 2: ", sum([x.output for x in displays]))


if __name__ == "__main__":
    main('input.txt')
    # main('test.txt')
```

Finally, we put it all together. Make the displays and then for part one we sum
up the total digits that are _knowns_ (1, 4, 7, and 8) and for part two we just
sum the displays' `output`s.

I might implement a print function so that we can get a cool seven segment
output for each display and/or segment. The challenge would be smashing four
multi-line strings so that each line in each string lines up with the same
indexed line of each other string. But if I add that, I'll update this post.
