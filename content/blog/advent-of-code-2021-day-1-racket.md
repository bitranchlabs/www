+++
date = 2021-12-01
title = 'Advent of Code 2021: Day 1 -- Racket'
[taxonimies]
tags = ["aoc", "racket"]
+++
I probably will regret pulling out Racket so early into the game, but I just felt like a lisp would be a fun solution to this challenge. So, while I will regret it, I enjoyed using a lisp (I'm fairly certain I won't let myself use _two_ Lisps during this challenge, I might change my mind though and grab Clojure or some not-Schemish Lisp later on if the going gets tough).

Today's challenge was comparing numbers if an array and if the previous number is smaller than the current number, increment a counter by one. As a disciple of Functional Programming, I knew that this was an obvious and trivial oneliner. I ended up making a bit of a mess of it though, because instead of ending up with a one liner I ended up with two deeply nested solutions. There are definitely better ways, but what I ended up doing was zipping two lists together such that the element in the first list is the previous element of the element in the second -- if the original list was `'( 1 2 3)` than the zipped list would be `'('(1 1) '(1 2) '(2 3))`. I duplicate the first element, because the challenge says the first element should compare to nothing and return 0 and I dropped the last element, since there is nothing to compare it to in the future. Than it was just a matter of calling `foldl` and looking at the two values in the pair. Here is my commented solution:

```racket
; Import the racket language module
#lang racket

; read input file to a list
(define i (file->list "input.txt"))

;Part One
; fold left, a poor man's reduce.  foldl takes three inputs, a function, the
; initial value, and the list to iterate through.
(foldl 
  ; anon func, takes the current list value (in our case a pair of numbers) and
  ; fold's accumulator
  (λ (xs acc)
      ; if the first item in the pair (the previous reading) is less than the
      ; second item (the current reading) increment the accumulator by 1,
      ; otherwise just return the accumulator
      (if (< (first xs) (second xs)) (+ 1 acc ) acc))
  ; 0 is the initial value of the accumulator.
  0
  ; map over items in _two_ lists, running list on each and returning that new
  ; list of pairs in a new list.
  (map list
        ; we need a new list that has the _first_ value in the original list
        ; twice, and the _final_ value in the original list dropped. So we get
        ; the first value of i (car i) and then we get all but the last element
        ; of i. This is a little convoluted in Scheme/Racket, so we have to
        ; reverse the list, than tail it (getting all but the first value, which
        ; is now the last value of i) andthen we _unreverse_ the list to get it
        ; back in the original order, sans the last value
        (append (list (car i)) (reverse (cdr (reverse i))))
        i))

;Part Two
; This is nearly identical to part one, except we have to iterate over three
; fewer elements! This is given a floating window of '(a b c d), we will get
; back '(a b c) and '(b c d).  The values b and c are the same, so the thing
; that determines which value is greater is the first element of the previous
; window and the last element of the last window. Thus, we take input, zip
; together the two lists again, but this time one list starts at the 4th value
; and the other list is missing the last 3 values. Everything below is the same
; as above except where I have added additional comments.
(foldl
  (λ (xs acc)
      (if (< (first xs) (second xs)) (+ 1 acc ) acc))
  0 (map list
          ; Reverse the list, drop the three elements (which are now at the
          ; head, but were at the tail before) and then reverse the list to
          ; revert to the original order.
          (reverse (drop (reverse i) 3))
          ; Drop the first three elements
          (drop i 3)))
```

A comment-less solution, my input, and a Dockerfile that runs this whole thing can be found in my [aoc2021](https://github.com/pard68/miscellanea/tree/master/aoc/2021 "Advent of Code 2021 repo") repo on github. I should probably wrap all my various aoc repos up into a single one, but that sounds like a lot of work... maybe after the holidays!
