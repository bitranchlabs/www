+++
title = "Advent of Code 2021: Day 3 -- J"
date = 2021-12-02
[taxonimies]
tags = ["aoc", "J"]
+++

I don't have much to say today. I did the problem in Python first to get my
head around the _how_. Then I rewrote it in J. You can see both the Python
solution to part 1 and the entire solution in J on my Github. I'll post some of
the J below. Honestly, J seems like a cool language, but the "tacit" part of
the langauge seems like a horrible idea. J _can_ be written like any other
language, but it has this "tacit mode" where you basically are writing APL with
ASCII chars. What you end up with is a verbose, and unreadable nastiness. I
wrote it tacitly. The first part was both fun and sort of understandable. The
second part... I hate it and I couldn't coax it into doing it the way that I
wanted it. Oh well, I got it done.

Also, weird to me, it seems J operates on 2D arrays ... oddly. So I was expecting to have to transpose the array and then get the sum, but that was returning what you'd have expected otherwise, so instead I just summed the 2D array and got what I was expecting. Weird, right? Without further ado, and with limited commentary... J:

```J
i=:"."0>cutopen 1!:1<'input.txt' NB. Input into a 2D array of ints
C=:(+/>:#-+/) NB. The common fn, takes a list of binaries, returns one binary # w/ most common bits from each
I=:-. NB. Invert a binary number
T=:#. NB. Binary to Decimal
p1 =: (C i) *&T (I C i) NB. Convert both to decimal and get product

CI =: 1 : 0  NB. Create function Common-iterator which takes y and iterates over it
	v =. (#~ (u (>: (# y) - ]) x { +/ y) = x&{"1) y
	(>:x) (T@:]`(u CI)@.(1 < # v)) v
)

p2 =: 0 (] CI *I CI) i
p1;p2
```

And the obligatory J-oneliners:
```
(C I)*&#.(-.(C=.(+/>:#-+/))(I=:"."0>cutopen 1!:1<'input.txt')) NB. Part 1 oneliner
0(]CI*-.(CI=.1 :('v=.(#~(u(>:(#y)-])x{+/y)=x&{"1)y';'(>:x)(#.@:]`(u CI)@.(1<#v))v')))I NB. Part 2 oneliner
```

Don't get me wrong. I had fun, but this is bad!!!
