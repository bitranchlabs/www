---
date: 2021-12-05
title: 'Advent of Code 2021: An Update'

---
I am still in the game. I finished day four yesterday and am working on day five today. However, I don't have much time on the weekends for this sort of stuff, so I am doing these challenges in Python. I will (hopefully) rewrite them later in the week to continue with my polyglot challenge, and I will write new posts for these days sometime this week, regardless of if the Python solutions get a rewrite or not.

👋